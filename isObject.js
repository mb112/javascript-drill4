function isObject (testObject){
    if (Array.isArray(testObject) || testObject === null || typeof testObject === "string" || typeof testObject === "number"){
        return false
    }else {
        return true
    }
}

module.exports = isObject