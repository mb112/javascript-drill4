const testObject = require('./objects')
const mapObject = require('./mapObject')

console.log('Test case 1 =',mapObject(testObject, function (a) {
    if (typeof a === 'string') {
        return a + '_string added'
    } else if (typeof a === 'number') {
        return a + 10
    }
}))
console.log('Test case 2 =',mapObject([], function (a) {
    if (typeof a === 'string') {
        return a + '_string added'
    } else if (typeof a === 'number') {
        return a + 10
    }
}))
console.log('Test case 3 =',mapObject(testObject,null ))
