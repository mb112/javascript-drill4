const testObject = require('./objects')
const invert = require('./invert')

console.log('Test case 1 = ', invert(testObject))
console.log('Test case 2 = ', invert(null))
console.log('Test case 3 = ', invert({}))
console.log('Test case 4 = ', invert({
    name: 'Bruce Wayne', age: 36, location: 'Gotham', parents: {
        father: 'Thomas Wayne',
        mother: 'Martha Wayne'
    }
}))