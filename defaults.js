const isObject = require('./isObject')

function defaults(testObject, defaultProps) {
    if (isObject(testObject) && isObject(defaultProps)) {
        for (let i in defaultProps) {
            if (testObject[i] === undefined) {
                testObject[i] = defaultProps[i]
            }
        }
        return testObject
    }
    return 'Invalid input '
}

module.exports = defaults