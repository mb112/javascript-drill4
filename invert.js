const isObject = require('./isObject')

function invert(testObject) {
    let invertedObject = {}
    if (isObject(testObject)) {
        for (let i in testObject) {
            if (isObject(testObject[i])) {
                invertedObject = Object.assign(invertedObject,invert(testObject[i]))

            } else {
                invertedObject[testObject[i]] = i
            }
        }
    }
    return invertedObject
}

module.exports = invert