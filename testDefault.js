const testObject = require('./objects')
const defaults = require('./defaults')


console.log('Test case 1 = ', defaults(testObject, {name: 'Bruce Wayne', father: 'Thomas Wayne', mother:'Martha Wayne', Nature: 'Philanthropist'}))
console.log('Test case 2 = ', defaults({}, {name: 'Bruce Wayne', father: 'Thomas Wayne', mother:'Martha Wayne', Nature: 'Philanthropist'}))
console.log('Test case 3 = ', defaults(testObject, {}))
console.log('Test case 4 = ', defaults({}, {}))
console.log('Test case 5 = ', defaults(null, {}))
console.log('Test case 6 = ', defaults(null, null))
console.log('Test case 7 = ', defaults({}, null))