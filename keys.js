const isObject = require('./isObject')


function keys(testObject) {
    objectKeys = []
    if (isObject(testObject)) {
        for (let i in testObject) {
            objectKeys.push(i)
        }
    }
    return objectKeys
}


module.exports = keys
