const isObject = require('./isObject')

function mapObject(testObject, cb) {
    let mappedObject = {}
    if (isObject(testObject) && cb) {
        for (let i in testObject) {
            mappedObject[i] = cb(testObject[i])
        }
    }
    return mappedObject
}

module.exports = mapObject