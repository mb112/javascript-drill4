const isObject = require('./isObject')


function pairs(testobject) {
    let keyValuePair = []
    if (isObject(testobject)) {
        for (let i in testobject) {
            keyValuePair.push([i, testobject[i]])
        }
    }
    return keyValuePair
}

module.exports = pairs